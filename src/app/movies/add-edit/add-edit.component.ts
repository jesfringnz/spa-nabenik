import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from 'src/app/data/models/movie/movie';
import { MovieService } from 'src/app/data/services/movie/movie.service';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})
export class AddEditComponent implements OnInit {

  form = new FormGroup({
    title: new FormControl('', Validators.required),
    year: new FormControl('', Validators.required),
    duration: new FormControl('', Validators.required)
  })

  id!: string;

  constructor(private route: ActivatedRoute, private movieService : MovieService, private rout: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    if (this.id) {
      this.movieService.getMovieById(this.id).subscribe(response =>{
        if(response){
          this.form.get('title')?.setValue(response.title)
          this.form.get('year')?.setValue(response.year)
          this.form.get('duration')?.setValue(response.duration)
        }
      })
    }
  }

  onSubmit() {
    if (this.id) {
      this.updateMovie()
    } else {
      this.createMovie()
    }
  }

  createMovie() {
    if (this.form.valid) {
      let movie : Movie = new Movie()
      movie.title=this.form.get('title')?.value
      movie.year=this.form.get('year')?.value
      movie.duration=this.form.get('duration')?.value
      this.movieService.createMovie(movie).subscribe( response =>{
        this.form.reset()
        alert("Pelicula creada")
        this.rout.navigate(['/movies'])
      }, error =>{
        alert("No se pudo crear la pelicula")
      })
    } else {
      alert("Existen campos sin completarse")
    }
  }

  updateMovie() {
    if (this.form.valid) {
      var result = confirm("Do you want to update this movie?")
      if(result){
        var movie = new Movie()
        movie.movieId = parseInt(this.id)
        movie.title = this.form.get('title')?.value
        movie.year = this.form.get('year')?.value
        movie.duration=this.form.get('duration')?.value
        this.movieService.updateMovie(movie).subscribe(response =>{
        this.form.reset()
          alert("Updated movie")
        this.rout.navigate(['/movies'])
        })
      }
    } else {
      alert("Required fields are missing")
    }
  }


  get f() {
    return this.form.controls;
  }
}
