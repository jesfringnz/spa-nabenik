import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddEditComponent } from './add-edit/add-edit.component';
import { MoviesRoutingModule } from './movies-routing.module';

@NgModule({
  declarations: [

    LayoutComponent,
       ListComponent,
       AddEditComponent,
       AddEditComponent
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class MoviesModule { }
