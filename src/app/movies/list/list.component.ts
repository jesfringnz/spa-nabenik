import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Movie } from 'src/app/data/models/movie/movie';
import { MovieService } from 'src/app/data/services/movie/movie.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  movies!: Movie[];
  title : string;

  
  constructor(private movieService: MovieService) {

  }

  ngOnInit(): void {

  }

  filterMovies() {
    if ( this.title != null) {
      this.movieService.listMoviesByTitle(this.title).subscribe(response => {
        this.movies = response;
      }, error => {
        alert("Unable to retrieve the movies")
      })
    }

  }

  deleteMovie(idMovie: number) {
    var result = confirm("Want to delete?");
    if(result){
      this.movieService.deleteMovie(idMovie).subscribe(response =>{
        const mov = this.movies.find(x => x.movieId === idMovie)
        if(mov){
          let index =this.movies.indexOf(mov)
          this.movies.splice(index,1)
        } 
        alert("Film deleted")
  
      },error =>{
        alert("Could not delete the movie")
      }
      )
    }

  }



}
