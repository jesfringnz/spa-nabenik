export class Movie {

    actorList: any[];
    duration: string;
    movieId: number;
    title: string;
    year: string;
    
}
