import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../../models/movie/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private routeMovie = 'http://localhost:8080/back-test/rest/movies';

  constructor(private http: HttpClient) {
    
   }

   listMoviesByTitle(title : any) : Observable<Movie[]>{
      let params = new HttpParams().set('title',title);
      let options = {params:params}
      return this.http.get<Movie[]>(this.routeMovie,options);
   }

   deleteMovie(id : any) : Observable<any>{
      return this.http.delete(this.routeMovie+`/${id}`);
   }

   getMovieById(id : any) : Observable<Movie>{
    return this.http.get<Movie>(this.routeMovie+`/${id}`);
   }

   updateMovie(movie : Movie) : Observable<any>{
    return this.http.post<any>(this.routeMovie+`/${movie.movieId}`,movie)
   }

   createMovie(movie : Movie) : Observable<any>{
      return this.http.put<any>(this.routeMovie,movie);
   }

}
