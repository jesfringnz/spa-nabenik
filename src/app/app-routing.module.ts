import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MoviesRoutingModule } from './movies/movies-routing.module';


const moviesModule = () => import('./movies/movies.module').then(x => x.MoviesModule);
const actorsModule = () => import('./actors/actors.module').then(x => x.ActorsModule);

const routes: Routes = [
  { path: '', component : HomeComponent},
  { path: 'movies', loadChildren:moviesModule},
  { path: 'actors', loadChildren:actorsModule},
  { path: '**',redirectTo:''}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
